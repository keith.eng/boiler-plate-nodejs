import mongo from "mongodb";

const uri = `mongodb+srv://${process.env.MONGO_DB_USER}:${process.env.MONGO_DB_PASSWORD}@cluster0.bbiva.mongodb.net/telegramFamilyBotDB?retryWrites=true&w=majority`;

const client = new mongo.MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

export async function run() {
  try {
    await client.connect();
    const database = client.db(process.env.MONGO_DB);
  } finally {
    await client.close();
  }
}

run().catch(console.dir);
