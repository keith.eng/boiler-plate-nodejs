import express from "express";
import { run } from "./config/database.js";

var app = express();

app.get("/", function (req, res) {
  res.send("Hello World");
});

var server = app.listen(8081, function () {
  var host = "localhost";
  var port = server.address().port;
  run();

  console.log("Example app listening at http://%s:%s", host, port);
});
